class DummyData {
  static var tickets = [
    {
      "id": 1,
      "title": "App crashed when opening settings",
      "description":
          "Whenever I try to open the settings screen, the app crashes.",
      "status": "OPEN",
      "created_at": "2024-01-27T08:00:00Z",
      "updated_at": "2024-01-27T08:15:00Z"
    },
    {
      "id": 2,
      "title": "Unable to login",
      "description":
          "I'm unable to login to my account. It keeps showing an error message.",
      "status": "OPEN",
      "created_at": "2024-01-26T10:30:00Z",
      "updated_at": "2024-01-26T11:45:00Z"
    },
    {
      "id": 3,
      "title": "Feature request: Dark mode",
      "description":
          "It would be great to have a dark mode option in the app for better readability at night.",
      "status": "IN_PROGRESS",
      "created_at": "2024-01-25T14:20:00Z",
      "updated_at": "2024-01-26T09:10:00Z"
    },
    {
      "id": 4,
      "title": "Bug: Images not loading in chat",
      "description": "Images are not loading in the chat section of the app.",
      "status": "RESOLVED",
      "created_at": "2024-01-24T16:45:00Z",
      "updated_at": "2024-01-25T12:30:00Z"
    },
    {
      "id": 5,
      "title": "Feature request: Push notifications",
      "description":
          "Please add push notifications feature to the app for real-time updates.",
      "status": "OPEN",
      "created_at": "2024-01-23T09:15:00Z",
      "updated_at": "2024-01-23T10:20:00Z"
    },
    {
      "id": 6,
      "title": "Bug: Profile picture not updating",
      "description":
          "I updated my profile picture but it's still showing the old one.",
      "status": "OPEN",
      "created_at": "2024-01-22T12:40:00Z",
      "updated_at": "2024-01-22T13:55:00Z"
    },
    {
      "id": 7,
      "title": "Feature request: Multi-language support",
      "description":
          "It would be helpful to have support for multiple languages in the app.",
      "status": "IN_PROGRESS",
      "created_at": "2024-01-21T15:30:00Z",
      "updated_at": "2024-01-22T08:45:00Z"
    },
    {
      "id": 8,
      "title": "Bug: Search functionality not working",
      "description":
          "When I try to search for something, no results are shown.",
      "status": "RESOLVED",
      "created_at": "2024-01-20T17:50:00Z",
      "updated_at": "2024-01-21T11:25:00Z"
    },
    {
      "id": 9,
      "title": "Feature request: Dark mode",
      "description":
          "Please add a dark mode option to the app for better night-time usage.",
      "status": "OPEN",
      "created_at": "2024-01-19T10:20:00Z",
      "updated_at": "2024-01-19T11:35:00Z"
    },
    {
      "id": 10,
      "title": "Bug: App freezes on startup",
      "description": "The app freezes every time I try to open it.",
      "status": "RESOLVED",
      "created_at": "2024-01-18T14:10:00Z",
      "updated_at": "2024-01-19T09:30:00Z"
    },
    {
      "id": 11,
      "title": "Feature request: Offline mode",
      "description": "It would be great to have an offline mode for the app.",
      "status": "IN_PROGRESS",
      "created_at": "2024-01-17T16:25:00Z",
      "updated_at": "2024-01-18T10:40:00Z"
    },
    {
      "id": 12,
      "title": "Bug: Payment not processing",
      "description":
          "I'm unable to complete the payment process. It keeps showing an error.",
      "status": "OPEN",
      "created_at": "2024-01-16T18:50:00Z",
      "updated_at": "2024-01-16T20:15:00Z"
    },
    {
      "id": 13,
      "title": "Feature request: Voice search",
      "description":
          "Adding voice search functionality would make it easier to find things in the app.",
      "status": "OPEN",
      "created_at": "2024-01-15T09:30:00Z",
      "updated_at": "2024-01-15T11:00:00Z"
    },
    {
      "id": 14,
      "title": "Bug: Videos not playing",
      "description": "Videos are not playing properly in the app.",
      "status": "IN_PROGRESS",
      "created_at": "2024-01-14T11:45:00Z",
      "updated_at": "2024-01-15T08:20:00Z"
    },
    {
      "id": 15,
      "title": "Feature request: Improved search filters",
      "description":
          "Please add more filters to the search functionality for better results.",
      "status": "RESOLVED",
      "created_at": "2024-01-13T14:20:00Z",
      "updated_at": "2024-01-14T10:30:00Z"
    },
    {
      "id": 16,
      "title": "Bug: Incorrect data displayed",
      "description": "The data displayed on the screen is incorrect.",
      "status": "OPEN",
      "created_at": "2024-01-12T16:40:00Z",
      "updated_at": "2024-01-12T18:55:00Z"
    },
    {
      "id": 17,
      "title": "Feature request: Sync across devices",
      "description":
          "It would be useful to sync data across multiple devices using the app.",
      "status": "OPEN",
      "created_at": "2024-01-11T19:00:00Z",
      "updated_at": "2024-01-11T20:25:00Z"
    },
    {
      "id": 18,
      "title": "Bug: Notifications not received",
      "description": "I'm not receiving any notifications from the app.",
      "status": "IN_PROGRESS",
      "created_at": "2024-01-10T08:45:00Z",
      "updated_at": "2024-01-11T07:10:00Z"
    },
    {
      "id": 19,
      "title": "Feature request: Improved UI",
      "description":
          "Please update the app's UI to make it more modern and intuitive.",
      "status": "OPEN",
      "created_at": "2024-01-09T10:30:00Z",
      "updated_at": "2024-01-09T12:00:00Z"
    },
    {
      "id": 20,
      "title": "Bug: App crashes when uploading images",
      "description": "The app crashes every time I try to upload images.",
      "status": "RESOLVED",
      "created_at": "2024-01-08T13:20:00Z",
      "updated_at": "2024-01-09T09:45:00Z"
    }
  ];
}
