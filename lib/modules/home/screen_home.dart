import 'package:flutter/material.dart';

import '../../models/model_ticket.dart';
import '../../utils/dummy_data.dart';
import '../../widgets/widget_ticket_card.dart';

class ScreenHome extends StatefulWidget {
  const ScreenHome({super.key});

  @override
  State<ScreenHome> createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  List<ModelTicket> tickets = [];
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
    loadData();
  }

  void loadData() {
    setState(() {
      isLoading = true;
    });
    tickets = DummyData.tickets.map((e) => ModelTicket.fromJson(e)).toList();
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: ListView.builder(
            itemCount: tickets.length,
            itemBuilder: (context, index) {
              return WidgetTicketCard(
                ticket: tickets[index],
              );
            }));
  }
}
