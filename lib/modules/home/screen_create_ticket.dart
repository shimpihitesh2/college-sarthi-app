import 'dart:io';

import 'package:college_sarthi/widgets/package_button.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ScreenCreateTicket extends StatefulWidget {
  @override
  _ScreenCreateTicketState createState() => _ScreenCreateTicketState();
}

class _ScreenCreateTicketState extends State<ScreenCreateTicket> {
  TextEditingController _queryController = TextEditingController();
  List<String> _attachedImages = []; // List to store attached images

  @override
  void dispose() {
    _queryController.dispose();
    super.dispose();
  }

  Future<void> _attachImage(ImageSource source) async {
    if (_attachedImages.length >= 5) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('You can attach maximum 5 images'),
      ));
      return;
    }
    final picker = ImagePicker();
    if (source == ImageSource.camera) {
      // Check if camera permission is granted
      // If not, request for permission
      final pickedImage = await picker.pickImage(source: source);
      setState(() {
        _attachedImages.add(pickedImage!.path);
      });
    } else {
      final pickedImages = await picker.pickMultiImage();
      if (pickedImages != null) {
        setState(() {
          _attachedImages
              .addAll(pickedImages.map((image) => image.path).toList());
        });
      }
    }
  }

  Widget _buildImagePreview(String image) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(right: 10),
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: Colors.grey.shade200,
          ),
          child: Center(child: Image.file(File(image), fit: BoxFit.cover)),
        ),
        Positioned(
          top: 0,
          right: 0,
          child: GestureDetector(
            onTap: () {
              setState(() {
                _attachedImages.remove(image);
              });
            },
            child: Icon(Icons.close, color: Colors.red),
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Enter Query'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              controller: _queryController,
              decoration: InputDecoration(
                labelText: 'Enter your query',
                border: OutlineInputBorder(),
              ),
              maxLines: 5,
            ),
            SizedBox(height: 20),
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.photo_camera),
                  onPressed: () => _attachImage(ImageSource.camera),
                ),
                IconButton(
                  icon: Icon(Icons.photo),
                  onPressed: () => _attachImage(ImageSource.gallery),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: _attachedImages
                          .map((image) => _buildImagePreview(image))
                          .toList(),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            PackageButton(
              title: "Submit",
            )
          ],
        ),
      ),
    );
  }
}
