import 'package:flutter/material.dart';
import 'package:flutter_phosphor_icons/flutter_phosphor_icons.dart';

import '../../utils/color_theme.dart';
import 'screen_create_ticket.dart';
import 'screen_home.dart';
import 'screen_profile.dart';

class ScreenMain extends StatefulWidget {
  const ScreenMain({super.key});

  @override
  State<ScreenMain> createState() => _ScreenMainState();
}

class _ScreenMainState extends State<ScreenMain> {
  final PageController pageController = PageController();
  int currentIndex = 0;
  List<String> titles = ["Home", "Profile"];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          automaticallyImplyLeading: false,
          backgroundColor: ColorTheme.primary,
          title: Text(titles[currentIndex],
              style: TextStyle(
                color: Colors.white,
                fontFamily: 'Inter',
                fontSize: 20,
                fontWeight: FontWeight.w700,
              )),
        ),
        body: PageView(
          controller: pageController,
          children: [ScreenHome(), ScreenProfile()],
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
            backgroundColor: ColorTheme.primary,
            shape: ShapeBorder.lerp(CircleBorder(), null, 0.0),
            child: Icon(
              Icons.add,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => ScreenCreateTicket()));
            }),
        bottomNavigationBar: BottomAppBar(
            elevation: 10,
            height: 75,
            surfaceTintColor: ColorTheme.primary,
            notchMargin: 10,
            shape: CircularNotchedRectangle(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    IconButton(
                        onPressed: () {
                          setState(() {
                            currentIndex = 0;
                          });
                          pageController.animateToPage(0,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.ease);
                        },
                        icon: Icon(
                          PhosphorIcons.ticket,
                          color: currentIndex == 0
                              ? ColorTheme.primary
                              : Colors.grey.shade400,
                        )),
                  ],
                ),
                IconButton(
                    onPressed: () {
                      setState(() {
                        currentIndex = 1;
                      });
                      pageController.animateToPage(1,
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.ease);
                    },
                    icon: Icon(
                      PhosphorIcons.user,
                      color: currentIndex == 1
                          ? ColorTheme.primary
                          : Colors.grey.shade400,
                    )),
              ],
            )));
  }
}
