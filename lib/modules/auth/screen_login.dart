import 'package:flutter/material.dart';

import '../../widgets/package_button.dart';
import '../home/screen_main.dart';
import '../../widgets/package_text_field.dart';
import '../../widgets/widget_auth_bottom_line.dart';
import 'screen_signup.dart';

class ScreenLogin extends StatelessWidget {
  const ScreenLogin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                PackageTextField(
                  hintText: "Email",
                ),
                PackageTextField(
                  hintText: "Password",
                ),
                SizedBox(
                  height: 50,
                ),
                PackageButton(
                  title: "Login",
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => ScreenMain()));
                  },
                ),
                WidgetAuthBottomLine(
                    title: "Don't Have an Account?",
                    actionText: "Sign Up",
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ScreenSignup()));
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
