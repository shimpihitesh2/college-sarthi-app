import 'package:flutter/material.dart';

import '../../widgets/package_button.dart';
import '../../widgets/package_text_field.dart';
import '../../widgets/widget_auth_bottom_line.dart';
import 'screen_login.dart';

class ScreenSignup extends StatefulWidget {
  const ScreenSignup({Key? key}) : super(key: key);

  @override
  State<ScreenSignup> createState() => _ScreenSignupState();
}

class _ScreenSignupState extends State<ScreenSignup> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: SingleChildScrollView(
            child: Column(
              children: [
                PackageTextField(
                  hintText: "FirstName",
                ),
                PackageTextField(
                  hintText: "Last Name",
                ),
                PackageTextField(
                  hintText: "Email",
                ),
                PackageTextField(
                  hintText: "Mobile Number",
                ),
                PackageTextField(
                  hintText: "Password",
                ),
                PackageTextField(
                  hintText: "Confirm Password",
                ),
                SizedBox(
                  height: 50,
                ),
                PackageButton(
                  title: "Sign Up",
                  onPressed: () {},
                ),
                WidgetAuthBottomLine(
                    title: "Already Have an Account?",
                    actionText: "Login",
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ScreenLogin()));
                    }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
