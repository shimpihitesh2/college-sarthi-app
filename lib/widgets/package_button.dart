import 'package:flutter/material.dart';

class PackageButtonController {
  VoidCallback? success;
  VoidCallback? error;
  VoidCallback? loading;
  VoidCallback? reset;

  void dispose() {
    success = null;
    error = null;
    loading = null;
    reset = null;
  }
}

class PackageButton extends StatefulWidget {
  final String? title;
  final PackageButtonController? controller;
  final Function? onPressed;
  const PackageButton({
    Key? key,
    this.title,
    this.controller,
    this.onPressed,
  }) : super(key: key);

  @override
  State<PackageButton> createState() => _PackageButtonState();
}

class _PackageButtonState extends State<PackageButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: InkResponse(
        onTap: () {
          if (widget.onPressed != null) {
            widget.onPressed!();
          }
        },
        child: Ink(
          decoration: BoxDecoration(
            gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFF69AEA9),
                Color(0xFF3F8782)
              ], // Change colors as needed
            ),
            borderRadius: BorderRadius.circular(30.0),

            boxShadow: [
              BoxShadow(
                color: Color(0xFF69AEA9)
                    .withOpacity(0.5), // Adjust color and opacity
                spreadRadius: 2,
                blurRadius: 10,
                offset: Offset(0, 3),
              ),
            ], // Circular border
          ),
          child: Container(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                widget.title ?? "Button",
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Inter",
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
