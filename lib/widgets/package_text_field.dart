import 'package:flutter/material.dart';

import '../utils/color_theme.dart';

class PackageTextField extends StatefulWidget {
  final TextEditingController? controller;
  final String? hintText;
  final TextInputType? keyboardType;

  const PackageTextField({
    Key? key,
    this.controller,
    this.hintText = "Enter",
    this.keyboardType = TextInputType.text,
  }) : super(key: key);

  @override
  State<PackageTextField> createState() => _PackageTextFieldState();
}

class _PackageTextFieldState extends State<PackageTextField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextFormField(
        controller: widget.controller,
        keyboardType: widget.keyboardType,
        style: TextStyle(fontFamily: 'Inter'),
        decoration: InputDecoration(
          hintText: widget.hintText,
          contentPadding:
              EdgeInsets.symmetric(vertical: 15.0, horizontal: 20.0),
          border: OutlineInputBorder(
            borderRadius:
                BorderRadius.circular(10.0), // Adjust the radius as needed
            borderSide: BorderSide(color: ColorTheme.secondary, width: 1.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(color: ColorTheme.secondary, width: 1.0),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(10.0),
            borderSide: BorderSide(color: ColorTheme.secondary, width: 2.0),
          ),
        ),
      ),
    );
  }
}
