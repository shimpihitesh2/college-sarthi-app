import 'package:flutter/material.dart';

import '../models/model_ticket.dart';

class WidgetTicketCard extends StatelessWidget {
  final ModelTicket ticket;

  const WidgetTicketCard({Key? key, required this.ticket}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color statusColor;
    switch (ticket.status) {
      case 'OPEN':
        statusColor = Colors.red;
        break;
      case 'IN_PROGRESS':
        statusColor = Colors.orange;
        break;
      case 'RESOLVED':
        statusColor = Colors.green;
        break;
      default:
        statusColor = Colors.grey;
    }

    return Card(
      margin: EdgeInsets.all(10),
      child: ListTile(
        title: Text(ticket.title),
        subtitle: Text(ticket.description),
        trailing: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
          decoration: BoxDecoration(
            color: statusColor,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Text(
            ticket.status,
            style: TextStyle(color: Colors.white),
          ),
        ),
        onTap: () {
          // Add your onTap logic here
        },
      ),
    );
  }
}
