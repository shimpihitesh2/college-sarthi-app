class ModelTicket {
  final int id;
  final String title;
  final String description;
  final String status;
  final String createdAt;
  final String updatedAt;

  ModelTicket({
    required this.id,
    required this.title,
    required this.description,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
  });

  factory ModelTicket.fromJson(Map<String, dynamic> json) {
    return ModelTicket(
      id: json['id'],
      title: json['title'],
      description: json['description'],
      status: json['status'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
